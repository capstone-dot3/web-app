import React from "react";
import { Grid, Paper, Typography, Divider } from '@material-ui/core';

const styles = {
    Paper: {
        width: "100pw",
        padding: 15,
        marginTop: 10,
    },
    Typography:{
        padding:10,
    }
}

const About = () => {
    return(
    <Grid item sm={12}>
        <Paper style={styles.Paper}>
            <Typography variant="headline">AcaLink</Typography>
            <Typography variant="caption" gutterBottom={true}>The future is now.</Typography>
            <Divider/>
            <Typography variant="subheading"> Capstone Project 2018</Typography>
            <Typography variant="body2"> By Meggan Do, Brian Tran, Jacky Trinh, Rudin Gjebero</Typography>
        </Paper>
    </Grid>
    );
}
export default About;