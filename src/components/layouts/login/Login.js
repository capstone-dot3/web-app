import React from "react";
import { Paper, Typography, Button, Input } from "@material-ui/core";
import { Redirect } from 'react-router-dom';
import * as acalink from '../../services/acalink';
import './Login.css';

const styles = {
    Paper: {
        width: '400px',
        height: '250px',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10, bottom: 0, left: 0, right: 0
    },
    Typography: {
        padding: 10,
    }
}

class Login extends React.Component {
    componentDidMount(){
        localStorage.clear();
    }
    state = {
        redirect: false
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            this.setState({ redirect: false });
            return <Redirect to='/dashboard' />;
        }
    }

    onSubmit = async (e) => {
        e.preventDefault();
        const form = e.target;
        const data = new FormData(form);
        let response = await acalink.authenticate(data.get("username"), data.get("password"));
        this.setUserInfo(response);
    }

    setUserInfo = (respData) => {
        if (!(respData.name !== null && respData.name === undefined) && !(respData.name === null && respData.name !== undefined)) {
            localStorage.setItem('username', respData.name);
            localStorage.setItem('role', respData.attributes.role);
            localStorage.setItem('uid', respData.attributes.uid);
            this.setRedirect();
        } else {
            this.setState({ redirect: false });
        }
    }

    render() {
        const { redirect } = this.state;
        if (redirect) {
            console.log('redirecting');
            return <Redirect from="/" to="/dashboard" />;
        }
        return (
            <div>
                <section className='sectionStyle'>
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                        <section className='imageStyle'></section>
                        <section className='imageStyle2'></section>
                        <Paper style={styles.Paper} margintop='30%' marginright='20%' align="center">
                            <Typography variant="title" align="center" gutterBottom>AcaLink Login</Typography><br></br>
                            <form id="formLogin" onSubmit={this.onSubmit}>
                                <Input id="username" name="username" placeholder="Username" required style={{ width: '80%' }} /><br></br><br></br>
                                <Input id="password" name="password" type="password" placeholder="Password" required style={{ width: '80%' }} />
                                <br></br><br></br><br></br>
                                <Button style={{ margin: -5 }} type="submit" variant="contained" color="primary"><span>Login</span></Button>
                                {/* <Typography class="footer"><a href="">Forgot your password?</a></Typography> */}
                            </form>
                        </Paper>

                    </div>
                </section>

            </div>
        );
    }
}
export default Login;

