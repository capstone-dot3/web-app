import React, { Fragment } from 'react';
import { Paper, Tabs, Tab, CssBaseline } from '@material-ui/core/';

class TranscriptTabs extends React.Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { value } = this.state;
        return (
            <Fragment>
                <CssBaseline />
                <Paper>
                    <Tabs value={value} onChange={this.handleChange} centered>
                        <Tab label="Transcripts" />
                    </Tabs>
                </Paper>
            </Fragment>
        );
    }
}

export default TranscriptTabs;
