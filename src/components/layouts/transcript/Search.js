import React, { Fragment } from "react";
import { Paper, Grid, Button, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { TranscriptTabs } from './';
import * as acalink from '../../services/acalink';
import TextField from '@material-ui/core/TextField';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

window.html2canvas = html2canvas;

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  Paper: {
    margin: '0 auto',
    justifyItems: 'center',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

function getTranscriptInstance(completionDate, courseId, courseName, credit, grade) {
  return { completionDate, courseId, courseName, credit, grade };
}

class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      record: [],
      studentID: '',
      isStudent: localStorage.getItem("role") === "student" ? true : false
    }
    this.exportToPDF = this.exportToPDF.bind(this);
  }

  getTranscripts = async (e) => {
    e.preventDefault();
    let data = await acalink.queryAll();
    var transcript = null;
    data.response.forEach(e => {
      var courses = [];
      var ele = e.Record.courses;
      ele.forEach(element => {
        courses.push(getTranscriptInstance(element.completionDate, element.courseId,
          element.courseName, element.credit, element.grade, element.body, element.body));
      });
      if (transcript == null) {
        transcript = [{
          "id": e.Record.id, "institution": e.Record.institution
          , "studentId": e.Record.studentId, "courses": courses
        }];
      } else {
        transcript.push({
          "id": e.Record.id, "institution": e.Record.institution
          , "studentId": e.Record.studentId, "courses": courses
        });
      }
    })
    this.setState({ record: transcript })
  }
  getTranscriptsByStudentID = async (e) => {
    e.preventDefault();
    var studentIDs = this.state.studentID ? this.state.studentID : localStorage.uid;
    const data = await acalink.queryAllByStudentId(studentIDs);
    if ((data.response[0] === null) || (data.response[0] === undefined) || (data.response[0] === '')) {
      return;
    }
    var transcript = null;
    data.response.forEach(e => {
      var courses = [];
      var ele = e.Record.courses;
      ele.forEach(element => {
        courses.push(getTranscriptInstance(element.completionDate, element.courseId,
          element.courseName, element.credit, element.grade, element.body, element.body));
      });
      if (transcript == null) {
        transcript = [{
          "id": e.Record.id, "institution": e.Record.institution
          , "studentId": e.Record.studentId, "courses": courses
        }];
      } else {
        transcript.push({
          "id": e.Record.id, "institution": e.Record.institution
          , "studentId": e.Record.studentId, "courses": courses
        });
      }
    })
    this.setState({ record: transcript })

  }

  getStudentID(e) {
    this.setState({
      studentID: e.target.value
    });
  }

  exportToPDF = async (e) => {
    var pdf = new jsPDF('p', 'pt', 'a4');
    let arry = Array.from(document.getElementsByClassName('test1'));
    pdf.fromHTML(arry[0]);
    pdf.save(localStorage.username + ".pdf")
  }

  renderButton() {
    if (!this.state.isStudent) {
      return (
        <Button onClick={this.getTranscriptsByStudentID} type="submit" variant="contained" color="primary">Get Transcript By ID</Button>
      );
    }
  }

  renderButton2() {
    if (!this.state.isStudent) {
      return (
        <TextField
          required
          id="institutionId"
          label="Student ID"
          name="institutionValue"
          onChange={this.getStudentID.bind(this)}
          margin="normal"
        />
      );
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <TranscriptTabs />
        <br>
        </br>
        <Grid container spacing={8} alignItems="center">
          <Grid item xs="true">
            <Button onClick={this.state.isStudent ? this.getTranscriptsByStudentID : this.getTranscripts} type="submit" variant="contained" color="primary">Get Transcripts</Button>
          </Grid>
          <Grid item xs="true">
            {this.renderButton2()}
          </Grid>
          <Grid item xs="true">
            {this.renderButton()}
          </Grid>
          <Grid item xs="true">
            <Button onClick={this.exportToPDF} type="submit" variant="contained" color="primary">Export To PDF</Button>
          </Grid>
        </Grid>
        <div className='test1'>
          <Grid item m="true">
            {this.state.record.map(transcriptInstance => {
              return (
                <Paper className={classes.root}>
                  <Table className={classes.table}>
                    <TableHead>
                      <TableRow>
                        <TableCell>Transcript ID</TableCell>
                        <TableCell>Institution ID</TableCell>
                        <TableCell>Student ID</TableCell>
                      </TableRow>
                      <TableRow key={transcriptInstance.id}>
                        <TableCell component="th" scope="row">
                          {transcriptInstance.id}
                        </TableCell>
                        <TableCell>{transcriptInstance.institution}</TableCell>
                        <TableCell>{transcriptInstance.studentId}</TableCell>
                      </TableRow>
                    </TableHead>
                  </Table>
                  <Table className={classes.table}>

                    <TableHead>
                      <TableRow>
                        <TableCell>Completion Date</TableCell>
                        <TableCell>Course ID</TableCell>
                        <TableCell>Course Name</TableCell>
                        <TableCell>Credit</TableCell>
                        <TableCell>Grade</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {transcriptInstance.courses.map(course => {
                        return (
                          <TableRow key={course.id}>
                            <TableCell component="th" scope="row">
                              {course.completionDate}
                            </TableCell>
                            <TableCell>{course.courseId}</TableCell>
                            <TableCell>{course.courseName}</TableCell>
                            <TableCell>{course.credit}</TableCell>
                            <TableCell>{course.grade}</TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </Paper>
              );
            })}
          </Grid>
        </div>
      </Fragment>
    );
  }
};

Search.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles)(styles)(Search);