import Add from "./Add.js";
import Search from "./Search.js";
import TranscriptTabs from "./TranscriptTabs.js";


export {
    Add, Search, TranscriptTabs
}