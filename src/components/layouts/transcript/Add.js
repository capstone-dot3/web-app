import React, { Fragment } from "react";
import { Redirect } from 'react-router-dom';
import { Grid, Button, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import * as acalink from '../../services/acalink';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    textFieldCourse: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 150,
    },
    textFieldSkinny: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 100,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
});

class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            valuesCourseIDs: [],
            valuesCourseNames: [],
            valuesCredits: [],
            valuesGrades: [],
            valuesStatuses: [],
            valueInstitutionName: '',
            valueStudentID: '',
            redirectAdd: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this._handleTextFieldChange = this._handleTextFieldChange.bind(this);

    }

    appendNewEmptyCourseRow() {
        return this.state.valuesCourseIDs.map((el, i) =>
            <div key={i}>
                <FormGroup>
                    <Fragment>
                        <Grid container spacing={16} alignItems="center">
                            <Grid item xs={2}>
                                <TextField
                                    required
                                    key={i}
                                    variant="outlined"
                                    id="courseId"
                                    value={el || ''}
                                    label="Course ID"
                                    inputProps={{
                                        maxLength: 10,
                                    }}

                                    onChange={this.handleChange.bind(this, i)}
                                    margin="normal"
                                    name="courseId"
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <TextField
                                    required
                                    key={i}
                                    value={this.state.valuesCourseNames[i] || ''}
                                    variant="outlined"
                                    id="courseName"
                                    label="Course Name"
                                    margin="normal"
                                    name="courseName"
                                    onChange={this.handleChangeCourseName.bind(this, i)}

                                />
                            </Grid>
                            <Grid item xs={2}>
                                <TextField
                                    required
                                    key={i}
                                    variant="outlined"
                                    id="credits"
                                    type="number"
                                    value={this.state.valuesCredits[i] || ''}
                                    label="Credits"
                                    name="credits"
                                    onChange={this.handleChangeCredit.bind(this, i)}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <TextField
                                    required={true}
                                    key={i}
                                    type="number"
                                    variant="outlined"
                                    id="grade"
                                    label="Grade"
                                    value={this.state.valuesGrades[i] || ''}
                                    inputProps={{ min: "0", max: "100" }}
                                    onChange={this.handleChangeGrade.bind(this, i)}
                                    name="grade"
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <InputLabel htmlFor="age-native-helper">Status</InputLabel><br></br>
                                <NativeSelect
                                    required
                                    key={i}
                                    variant="outlined"
                                    id="status"
                                    label="Status"
                                    value={this.state.valuesStatuses[i] || ''}
                                    name="status"
                                    onChange={this.handleChangeStatus.bind(this, i)}
                                    margin="normal"
                                >
                                    <option value={''}></option>
                                    <option value={'COMPLETE'}>Complete</option>
                                    <option value={'INCOMPLETE'}>Incomplete</option>
                                    <option value={'WITHDRAWN'}>Withdrawn</option>
                                </NativeSelect>
                            </Grid>
                            <Grid >
                                <DeleteIcon onClick={this.deleteCourseRow.bind(this, i)} />
                            </Grid>
                        </Grid>

                    </Fragment>
                </FormGroup>
            </div>
        )
    }

    handleChange(i, event) {
        let valuesCourseIDs = [...this.state.valuesCourseIDs];
        valuesCourseIDs[i] = event.target.value;
        this.setState({ valuesCourseIDs });
    }

    handleChangeCourseName(i, event) {
        let valuesCourseNames = [...this.state.valuesCourseNames];
        valuesCourseNames[i] = event.target.value;
        this.setState({ valuesCourseNames });
    }

    handleChangeCredit(i, event) {
        let valuesCredits = [...this.state.valuesCredits];
        valuesCredits[i] = event.target.value;
        this.setState({ valuesCredits });
    }

    handleChangeGrade(i, event) {

        let valuesGrades = [...this.state.valuesGrades];

        valuesGrades[i] = event.target.value;
        this.setState({ valuesGrades });
    }

    handleChangeStatus(i, event) {
        let valuesStatuses = [...this.state.valuesStatuses];
        valuesStatuses[i] = event.target.value;
        this.setState({ valuesStatuses });
    }

    addClick() {
        this.setState(prevState => ({ valuesCourseIDs: [...prevState.valuesCourseIDs, ''] }))
    }

    populateMockData() {

        this.setState(this.state.valuesCourseIDs = ({
            valuesCourseIDs: ['INFO34049', 'MATH32668', 'PROG39402'],
            valuesCourseNames: ['Capstone Project Research Prep', 'Statistics – Computer Science', 'Advanced Mobile Application Development'],
            valuesCredits: ['6.0', '6.0', '4.0'],
            valuesGrades: ['76', '83', '72'],
            valuesStatuses: ["COMPLETE", "INCOMPLETE", 'WITHDRAWN'],
            valueInstitutionName: 'Sheridan College',
            valueStudentID: 991348290,
        }))
    }

    deleteCourseRow(i) {
        let valuesCourseIDs = [...this.state.valuesCourseIDs];
        valuesCourseIDs.splice(i, 1);
        this.setState({ valuesCourseIDs });
        let valuesCourseNames = [...this.state.valuesCourseNames];
        valuesCourseNames.splice(i, 1);
        this.setState({ valuesCourseNames });
        let valuesCredits = [...this.state.valuesCredits];
        valuesCredits.splice(i, 1);
        this.setState({ valuesCredits });
        let valuesGrades = [...this.state.valuesGrades];
        valuesGrades.splice(i, 1);
        this.setState({ valuesGrades });
        let valuesStatuses = [...this.state.valuesStatuses];
        valuesStatuses.splice(i, 1);
        this.setState({ valuesStatuses });
    }

    _handleTextFieldChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    validateIfFieldIsEmpty(x) {
        return (x === undefined || x === null || x.length <= 0) ? true : false;
    }

    validateGradeInput(x) {
        if ((x < 0) || (x > 100)) {
            return -1;
        } else if (x % 1 !== 0) {
            return 0;
        }
        return 1;
    }

    validateCreditInput(x) {
        if (x < 0) {
            alert('Credit must be above 0');
            return -1;
        }
        x = parseFloat(x).toFixed(1);

        return x;
    }

    handleSubmit = async (event) => {
        event.preventDefault();

        var dataCourseIDs = [];
        var dataCourseNames = [];
        var dataCredits = [];
        var dataGrades = [];
        var dataStatuses = [];

        var valueInstitutionName = this.state.valueInstitutionName;
        var valueStudentID = this.state.valueStudentID;

        if (this.state.valuesCourseIDs.length === 0) {
            alert('Please add Course before submitting')
            return;
        }

        for (var i = 0; i < this.state.valuesCourseIDs.length; i++) {
            dataCourseIDs[i] = this.state.valuesCourseIDs[i];
            dataCourseNames[i] = this.state.valuesCourseNames[i];
            dataCredits[i] = this.state.valuesCredits[i];
            dataGrades[i] = this.state.valuesGrades[i];
            dataStatuses[i] = this.state.valuesStatuses[i];




            if ((this.validateIfFieldIsEmpty(dataCourseIDs[i])) || (this.validateIfFieldIsEmpty(dataCourseNames[i])) || (this.validateIfFieldIsEmpty(dataCredits[i]))
                || (this.validateIfFieldIsEmpty(dataGrades[i])) || (this.validateIfFieldIsEmpty(dataStatuses[i]))) {
                alert('Fields are missing information')
                return;
            }

            if (this.validateGradeInput(dataGrades[i]) === -1) {
                alert('Grade must be between 0 and 100');
                return;
            } else if (this.validateGradeInput(dataGrades[i]) === 0) {
                alert('Grade must be a whole number');
                return;
            }

            if (this.validateCreditInput(dataCredits[i]) === -1) {
                return;
            } else {
                dataCredits[i] = this.validateCreditInput(dataCredits[i]);
            }
        }

        var transcriptDataJSON = "";

        transcriptDataJSON += '{"courses": [ ';


        for (var i = 0; i < this.state.valuesCourseIDs.length; i++) {
            transcriptDataJSON += '{"completionDate": ' + '"FALL2018",' +
                '"courseId": "' + dataCourseIDs[i] + '",' +
                '"courseName": "' + dataCourseNames[i] + '",' +
                '"credit": "' + String(dataCredits[i]) + '",' +
                '"grade": "' + String(dataGrades[i]) + '",' +
                '"status": "' + String(dataStatuses[i]) + '"}';


            if (i !== this.state.valuesCourseIDs.length - 1) {
                transcriptDataJSON += ',';
            }

        }

        transcriptDataJSON += " ],";

        transcriptDataJSON +=
            '"institution"' + ': "' + valueInstitutionName + '",' +
            '"username"' + ': "' + localStorage.username + '",' +
            '"studentId"' + ': "' + String(valueStudentID) + '"}';

        if (this.validateIfFieldIsEmpty(valueInstitutionName) || this.validateIfFieldIsEmpty(valueStudentID)) {
            alert('Fields are missing information')
            return;
        }
        let respData = await acalink.addTranscript(transcriptDataJSON);
        if (respData.response.id !== undefined) {
            alert('Successfully added a new transcript.\nID: ' + respData.response.id + '\nStudent ID: ' + respData.response.studentId);
            this.setState({ redirectAdd: true });
        }
    }

    render() {
        const { redirectAdd } = this.state;
        if (localStorage.role === "student") {
            this.setState({ redirectAdd: true });
        }
        if (redirectAdd) {
            this.setState({ redirectAdd: false });
            return <Redirect from="/add" to="/dashboard" />;
        }
        return (
            <div>
                <Typography variant="title">Add Transcript</Typography>
                <form noValidate autoComplete="off" >
                    <Grid container spacing={16}>
                        <Grid item xs>
                            <TextField
                                required={true}
                                id="institutionId"
                                label="Institution ID"
                                name="valueInstitutionName"
                                input="text"
                                value={this.state.valueInstitutionName}
                                onChange={this._handleTextFieldChange}
                                onSubmit={this.handleSubmit.bind(this)}
                                margin="normal"
                            />
                        </Grid>
                        <Grid item xs>
                            <TextField
                                required={true}
                                id="studentId"
                                label="Student ID"
                                type="number"
                                name="valueStudentID"
                                value={this.state.valueStudentID}
                                onChange={this._handleTextFieldChange}
                                margin="normal"
                            />
                        </Grid>
                        <Grid item xs>
                            <Button onClick={this.addClick.bind(this)} variant="contained" color="primary" >
                                Add Course
                                <Icon />
                            </Button>
                        </Grid>
                        <Grid item xs={2}>
                            <div onClick={this.populateMockData.bind(this)} style={{
                                backgroundColor: "transparent", height: 50, width: 200, cursor: "auto", boxSizing: "none",
                                boxShadow: "none", WebkitBoxShadow: "none", outlineColor: "none", MozBoxShadow: "none", color: (0, 255, 0, 0),
                            }} >

                                <Icon />
                            </div>
                        </Grid>
                    </Grid>
                </form>



                <form onSubmit={this.handleSubmit}>
                    {this.appendNewEmptyCourseRow()}
                    <br></br>
                    <Button onClick={this.handleSubmit.bind(this)} variant="contained" color="primary" >
                        Submit
                    <Icon />
                    </Button>
                </form>


            </div>


        );
    }
}

Add.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default (withStyles)(styles)(Add);
