import { fetchApi } from '../api';

const endPoints = {
    authenticate: '/api/enrollUser',
    logout: '/api/logoutUser',
    addTranscript: '/api/invoke/addtranscript',
    queryAll: '/api/queries/queryAll',
    queryAllByStudentId: '/api/queries/querybystudentid/p',
};

const header = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'Access-Control-Allow-Origin': '*'
}

export const authenticate = async (username, password) => await fetchApi(
    endPoints.authenticate,
    JSON.stringify({
        username: username,
        password: password,
    }),
    'POST',
    header
);

export const logout = async () => await fetchApi(
    endPoints.logout,
    JSON.stringify({
        username: localStorage.username,
    }),
    'POST',
    header
);

export const addTranscript = async (transcriptJSON) => await fetchApi(
    endPoints.addTranscript,
    transcriptJSON,
    'POST',
    header
);

export const queryAll = async () => await fetchApi(
    endPoints.queryAll,
    {
        username: localStorage.username
    }
);

export const queryAllByStudentId = async (studentId) => await fetchApi(
    endPoints.queryAllByStudentId,
    {
        studentID: studentId,
        username: localStorage.username
    }
);

