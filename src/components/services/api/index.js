function buildQueryParams(obj) {
	const keyValuePairs = [];
	for (const key in obj) {
	  keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
	}
	return '?' + keyValuePairs.join('&');
  }

export const fetchApi = async (endPoint, payload = {}, method = 'GET', headers = {}) => {
	try {
		let response = null;
		if (method === 'GET') {
			let queryParams = buildQueryParams(payload);
			response = await fetch(`${process.env.REACT_APP_API_URL}${endPoint}${queryParams}`, {
				method: method,
				headers: headers
			});
		} else if (method === 'POST') {
			response = await fetch(`${process.env.REACT_APP_API_URL}${endPoint}`, {
				method: method,
				headers: headers,
				body: payload,
			});
		}
		return response.json();
	}
	catch (e) {
		console.log('api prmise err' + e);
	}
};
