import React, { Fragment } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./components/layouts/login/Login";
import {Add, Search} from "./components/layouts/transcript";
import About from "./components/layouts/about/About";
import Error from "./components/layouts/error/Error";
import { Dashboard } from "./components/layouts";
import { CssBaseline } from '@material-ui/core';

class App extends React.Component {
  render() {
      return (
        <Fragment>
          <CssBaseline />
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Login} />
              <Dashboard>
                <Route path="/dashboard" render={
                  props => <Search {...props} />
                } >
                </Route>
                <Route path="/add" component={Add} />
                <Route path="/about" component={About} />
              </Dashboard>
              <Route component={Error} />
            </Switch>
          </BrowserRouter>
        </Fragment>
      )
  }
};

export default App;